const data = [{ "id": 1, "color": "Indigo", "material": "Plexiglass", "quantity": 49, "cost": "$0.56" },
{ "id": 2, "color": "Mauv", "material": "Stone", "quantity": 60, "cost": "$2.76" },
{ "id": 3, "color": "Aquamarine", "material": "Glass", "quantity": 82, "cost": "$9.03" },
{ "id": 4, "color": "Puce", "material": "Stone", "quantity": 69, "cost": "$0.42" },
{ "id": 5, "color": "Turquoise", "material": "Plastic", "quantity": 15, "cost": "$4.52" },
{ "id": 6, "color": "Violet", "material": "Vinyl", "quantity": 57, "cost": "$7.66" },
{ "id": 7, "color": "Indigo", "material": "Glass", "quantity": 13, "cost": "$6.23" },
{ "id": 8, "color": "Fuscia", "material": "Plastic", "quantity": 48, "cost": "$0.47" },
{ "id": 9, "color": "Yellow", "material": "Stone", "quantity": 1, "cost": "$2.76" },
{ "id": 10, "color": "Indigo", "material": "Rubber", "quantity": 87, "cost": "-$1.06" },
{ "id": 11, "color": "Violet", "material": "Granite", "quantity": 39, "cost": "$3.37a" },
{ "id": 12, "color": "Yellow", "material": "Wood", "quantity": 33, "cost": "$7.69" },
{ "id": 13, "color": "Red", "material": "Brass", "quantity": 54, "cost": "$4.28" },
{ "id": 14, "color": "Teal", "material": "Aluminum", "quantity": 56, "cost": "$2.78" },
{ "id": 15, "color": "Puce", "material": "Granite", "quantity": 32, "cost": "$1.09" },
{ "id": 16, "color": "Puce", "material": "Wood", "quantity": 13, "cost": "$9.61" },
{ "id": 17, "color": "Violet", "material": "Vinyl", "quantity": 51, "cost": "$3.10" },
{ "id": 18, "color": "Violet", "material": "Stone", "quantity": 24, "cost": "$0.15" },
{ "id": 19, "color": "Fuscia", "material": "Vinyl", "quantity": 91, "cost": "$8.22" },
{ "id": 20, "color": "Aquamarine", "material": "Rubber", "quantity": 15, "cost": "$2.28" },
{ "id": 21, "color": "Crimson", "material": "Glass", "quantity": 88, "cost": "$3.33" },
{ "id": 22, "color": "Orange", "material": "Wood", "quantity": 21, "cost": "$5.07" },
{ "id": 23, "color": "Orange", "material": "Stone", "quantity": 56, "cost": "$2.13" },
{ "id": 24, "color": "Puce", "material": "Steel", "quantity": 27, "cost": "$0.68" },
{ "id": 25, "color": "Teal", "material": "Rubber", "quantity": 44, "cost": "$2.46" },
{ "id": 26, "color": "Teal", "material": "Glass", "quantity": 64, "cost": "$8.74" },
{ "id": 27, "color": "Indigo", "material": "Plexiglass", "quantity": 1, "cost": "$1.05" },
{ "id": 28, "color": "Orange", "material": "Glass", "quantity": 58, "cost": "$1.32" },
{ "id": 29, "color": "Aquamarine", "material": "Stone", "quantity": 27, "cost": "$1.07" },
{ "id": 30, "color": "Indigo", "material": "Glass", "quantity": 89, "cost": "$5.82" }];

const result = data.map((material) => {
    material.cost = Number(material.cost.replace('$', '')
    .replace('a', ''));
    return material;
})
    .filter((material) => {
        return material.color === "Indigo";
    })
    .reduce((acc, material) => {
        if (!acc[material.color]) {
            acc[material.color] = (material.cost);
        } else {
            acc[material.color] += (material.cost);
        }

        return acc;
    }, {});

result['Indigo'] = parseFloat((result['Indigo']).toFixed(2));

console.log(result);
